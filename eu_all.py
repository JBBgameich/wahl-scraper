#!/usr/bin/env python3
import os
import shutil
from scraper import euScraper

# Extract data for all elections
dir = "results/eu"
if os.path.isdir(dir):
	shutil.rmtree(dir, ignore_errors=True)

os.makedirs(dir)

for election in range(0, int((2019 - 1979) / 5)):
	year = 1979 + election * 5
	print(year)
	euScraper.extract("https://www.election-results.eu/european-results/{}-{}/constitutive-session/".format(year, year + 5), dir + "/" + str(year) + ".json")

# Url for 2019 is different
year = 2019
print(year)
euScraper.extract("https://www.election-results.eu/european-results/{}-{}/".format(year, year + 5), dir + "/" + str(year) + ".json")

