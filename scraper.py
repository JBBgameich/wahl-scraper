#!/usr/bin/env python3

from bs4 import BeautifulSoup
import requests
import json
import os

class euScraper:
	def extract(url, out):
		# Request website
		result = requests.get(url).text

		# Parse website
		soup = BeautifulSoup(result, 'html.parser')

		# Find parties
		electionResultsSoup = soup.find("section", {"class": "breakdown-table--container"})
		partySoups = electionResultsSoup.findAll("tr", {"class": "breakdown-table--row breakdown-table--row--dotted"})

		resultsDict = {}

		# Get columns
		for party in partySoups:
			seatsSoup, percentageSoup = party.findAll("td", {"class": "breakdown-table--cell breakdown-table--cell--seats breakdown-table--column-2"})
			name = party.find("div", {"class": "breakdown-table--bar-container"}).strong.string
			seats = int(seatsSoup.strong.string)
			percentage = float(percentageSoup.strong.string.replace("%", ""))
			color_css = party.find("div", {"class": "breakdown-table--bar"})["style"].split(";")[0].split(":")[1].strip()
			color = tuple(int(color_css.lstrip("#")[i:i+2], 16) for i in (0, 2, 4))

			resultsDict[name] = {"seats": seats, "percentage": percentage * 0.01, "color": color}
			print(" " * 4, name, seats, str(percentage) + "%")

		# Write result
		outFile = open(out, "w")
		outFile.write(json.dumps(resultsDict, indent=4))
		outFile.close()

class btwScraper:
	def extract(url, out):
		# Request website
		request = requests.get(url)
		request.encoding = "utf-8"
		result = request.text

		# Parse website
		soup = BeautifulSoup(result, 'html.parser')

		# Find results (luckily already json!)
		electionSeatResults = json.loads(soup.find(id="sitze2table").div["data-chartdata"])["data"]
		electionPercentageResults = json.loads(soup.find(id="zweitstimmen-prozente10").div["data-chartdata"])["data"]
		#seatsAvailable = int(json.loads(soup.find(id="sitze2table").div["data-chartoptions"])["pie"]["centerLabel"].replace("Sitze", "").strip())

		resultsDict = {}

		# Convert json into our format
		for party in electionPercentageResults:
			name = party["label"]
			percentage = party["value"]

			color = tuple(int(party["color"].lstrip("#")[i:i+2], 16) for i in (0, 2, 4))

			resultsDict[name] = {"percentage": percentage * 0.01, "color": color}
			print(" " * 4, name, str(percentage) + "%")

		for party in electionSeatResults:
			name = party["label"]
			seats = party["value"]

			resultsDict[name]["seats"] = seats

		# Write result
		outFile = open(out, "w")
		outFile.write(json.dumps(resultsDict, indent=4))
		outFile.close()
