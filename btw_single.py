#!/usr/bin/env python3
import sys
from scraper import btwScraper

try:
	url = sys.argv[1]
	out = sys.argv[2]
except:
	print("No url or output filename provided")

btwScraper.extract(url, out)
