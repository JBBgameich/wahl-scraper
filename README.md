# wahl-scraper

Writes data from the european elections into json

# How to invoke

`./eu_single.py https://www.election-results.eu/european-results/2019-2024/`

`./eu_all.py`

or

`./btw_single.py https://www.bundeswahlleiter.de/bundestagswahlen/2017/ergebnisse.html`

`./btw_all.py`
