#!/usr/bin/env python3
import os
import shutil
from scraper import btwScraper

# Extract data for all elections
dir = "results/btw"
if os.path.isdir(dir):
	shutil.rmtree(dir, ignore_errors=True)

os.makedirs(dir)

for election in range(0, int((2021 - 2013) / 4)): # FIXME Only works if elections are really every 4 years
	year = 2013 + election * 4
	print(year)
	btwScraper.extract("https://www.bundeswahlleiter.de/bundestagswahlen/{}/ergebnisse.html".format(year), dir + "/" + str(year) + ".json") # FIXME Add support for parsing Erststimmen
