#!/usr/bin/env python3
import sys
from scraper import euScraper

try:
	url = sys.argv[1]
	out = sys.argv[2]
except:
	print("No url or output filename provided")

euScraper.extract(url, out)
